use bevy::{
    asset::{AssetLoader, LoadedAsset},
    prelude::*,
    reflect::{TypePath, TypeUuid},
};
use serde::Deserialize;

pub struct AssetsPlugin;

impl Plugin for AssetsPlugin {
    fn build(&self, app: &mut App) {
        app.add_asset::<AssetExemple>()
            .add_asset_loader(MyAssetLoader)
            .add_systems(Startup, test_setup_asset)
            .add_systems(Update, print_assets);
    }
}

fn test_setup_asset(asset_server: Res<AssetServer>) {
    let _: Handle<AssetExemple> = asset_server.load("test_file.toml");
}

fn print_assets(assets: Res<Assets<AssetExemple>>) {
    for (_, asset) in assets.iter() {
        println!("asset: {}", asset.str);
    }
}

#[derive(Debug, Deserialize, TypeUuid, TypePath, Clone)]
#[uuid = "45562ae0-66ad-11ee-8c99-0242ac120002"]
pub struct AssetExemple {
    pub str: String,
}

struct MyAssetLoader;
impl AssetLoader for MyAssetLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut bevy::asset::LoadContext,
    ) -> bevy::utils::BoxedFuture<'a, Result<(), bevy::asset::Error>> {
        Box::pin(async move {
            let serialized = toml::from_str::<AssetExemple>(&String::from_utf8(bytes.to_vec())?)?;
            load_context.set_default_asset(LoadedAsset::new(serialized));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["toml"]
    }
}
