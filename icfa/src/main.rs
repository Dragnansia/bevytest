mod assets;

use assets::AssetsPlugin;
use bevy::prelude::*;

fn main() {
    App::new().add_plugins((DefaultPlugins, AssetsPlugin)).run();
}
