mod environment;

use bevy::prelude::*;
use bevy_flycam::{KeyBindings, PlayerPlugin};
use environment::EnvironmentPlugins;

fn main() {
    App::new()
        .add_plugins((DefaultPlugins, EnvironmentPlugins, PlayerPlugin))
        .insert_resource(KeyBindings {
            move_left: KeyCode::Q,
            move_forward: KeyCode::Z,
            ..default()
        })
        .run();
}
