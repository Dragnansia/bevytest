use bevy::prelude::*;
use bevy_rapier3d::prelude::*;

pub struct BallPlugins;

impl Plugin for BallPlugins {
    fn build(&self, app: &mut App) {
        app.insert_resource(SpawnTimer(Timer::from_seconds(0.2, TimerMode::Repeating)))
            .add_systems(Update, (update_ball, check_destroy_ball));
    }
}

#[derive(Default, Resource)]
struct SpawnTimer(Timer);

#[derive(Component)]
struct Ball;

fn update_ball(
    mut commands: Commands,
    time: Res<Time>,
    mut timer: ResMut<SpawnTimer>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    if timer.0.tick(time.delta()).just_finished() {
        let elapsed = time.elapsed_seconds();

        commands
            .spawn(Ball)
            .insert(PbrBundle {
                mesh: meshes.add(
                    shape::UVSphere {
                        radius: 2.0,
                        ..default()
                    }
                    .into(),
                ),
                material: materials.add(Color::WHITE.into()),
                transform: Transform::from_xyz(0.0, 10.0, 0.0),
                ..default()
            })
            .insert(RigidBody::Dynamic)
            .insert(Velocity::linear(Vec3::new(
                elapsed.cos() * 50.0,
                0.0,
                elapsed.sin() * 50.0,
            )))
            .insert(Collider::ball(2.01));
    }
}

fn check_destroy_ball(
    mut commands: Commands,
    mut balls: Query<(Entity, Option<&mut Transform>), With<Ball>>,
) {
    for (entity, transform) in balls.iter_mut() {
        let Some(transform) = transform else { continue; };

        if transform.translation.y < -15.0 {
            commands.entity(entity).despawn();
        }
    }
}
