use bevy::prelude::*;
use bevy_rapier3d::prelude::*;

pub struct TerrainPlugins;

impl Plugin for TerrainPlugins {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, build_terrain);
    }
}

#[derive(Default, Component)]
struct Terrain;

fn build_terrain(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let size = 200_f32;
    commands
        .spawn(PbrBundle {
            mesh: meshes.add(shape::Plane::from_size(size).into()),
            material: materials.add(Color::rgb(0.3, 0.5, 0.3).into()),
            ..default()
        })
        .insert(Collider::cuboid(size / 2.0, 0.0, size / 2.0))
        .insert(Restitution::coefficient(0.7));
}
