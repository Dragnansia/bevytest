mod ball;
mod terrain;

use crate::ball::BallPlugins;
use crate::terrain::TerrainPlugins;
use bevy::prelude::*;
use bevy_flycam::prelude::*;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_rapier3d::prelude::*;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            WorldInspectorPlugin::new(),
            PlayerPlugin,
            TerrainPlugins,
            BallPlugins,
            RapierPhysicsPlugin::<NoUserData>::default(),
            RapierDebugRenderPlugin::default(),
        ))
        .insert_resource(KeyBindings {
            move_forward: KeyCode::Z,
            move_left: KeyCode::Q,
            ..default()
        })
        .run();
}
