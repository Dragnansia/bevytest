use bevy::prelude::*;
use bevy_atmosphere::prelude::*;
use bevy_flycam::FlyCam;
use std::time::Duration;

pub struct EnvironmentPlugin;

impl Plugin for EnvironmentPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(CycleTimer(Timer::new(
            Duration::from_millis(50),
            TimerMode::Repeating,
        )))
        .insert_resource(AtmosphereModel::default())
        .add_systems(Startup, build_environment)
        .add_systems(Update, daylight_cycle);
    }
}

#[derive(Component)]
struct Sun;

#[derive(Resource)]
struct CycleTimer(Timer);

fn build_environment(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    // Sun
    commands.spawn((
        DirectionalLightBundle {
            directional_light: DirectionalLight {
                shadows_enabled: true,
                ..default()
            },
            ..default()
        },
        Sun,
    ));

    commands.spawn(PbrBundle {
        mesh: meshes.add(shape::Cube::new(0.5).into()),
        material: materials.add(Color::rgb(0.8, 0., 0.).into()),
        transform: Transform::from_xyz(1., 0., 0.),
        ..default()
    });

    commands.spawn(PbrBundle {
        mesh: meshes.add(shape::Cube::new(0.5).into()),
        material: materials.add(Color::rgb(0., 0.8, 0.).into()),
        transform: Transform::from_xyz(0., 1., 0.),
        ..default()
    });

    commands.spawn(PbrBundle {
        mesh: meshes.add(shape::Cube::new(0.5).into()),
        material: materials.add(Color::rgb(0., 0., 0.8).into()),
        transform: Transform::from_xyz(0., 0., 1.),
        ..default()
    });

    commands.spawn((
        Camera3dBundle { ..default() },
        FlyCam,
        AtmosphereCamera::default(),
    ));
}

fn daylight_cycle(
    mut atmosphere: AtmosphereMut<Nishita>,
    mut query: Query<(&mut Transform, &mut DirectionalLight), With<Sun>>,
    mut timer: ResMut<CycleTimer>,
    time: Res<Time>,
) {
    timer.0.tick(time.delta());

    if timer.0.finished() {
        let t = time.elapsed_seconds_wrapped() / 2.0;
        atmosphere.sun_position = Vec3::new(0., t.sin(), t.cos());

        if let Some((mut light_trans, mut directional)) = query.single_mut().into() {
            light_trans.rotation = Quat::from_rotation_x(-t);
            directional.illuminance = t.sin().powf(2.) * 100000.;
        }
    }
}
