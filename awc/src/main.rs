mod environment;

use crate::environment::EnvironmentPlugin;
use bevy::prelude::*;
use bevy_atmosphere::prelude::*;
use bevy_flycam::prelude::*;
use bevy_hanabi::prelude::*;
use bevy_inspector_egui::quick::WorldInspectorPlugin;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            AtmospherePlugin,
            HanabiPlugin,
            NoCameraPlayerPlugin,
            EnvironmentPlugin,
            WorldInspectorPlugin::default(),
        ))
        .insert_resource(KeyBindings {
            move_forward: KeyCode::Z,
            move_left: KeyCode::Q,
            ..default()
        })
        .run();
}
